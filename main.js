const buttonOne = document.querySelector('#add-item');
const buttonGroup = document.querySelector('#add-group');
const template = document.querySelector('#li-template');
const list = document.querySelector('#list');

const createItem = () => {
  const item = template.content.cloneNode(true);
  item.querySelector('li').textContent = Date.now();
  return item;
}

const addItem = () => {
  list.append(createItem());
}

const addGroup = () => {
  const fragment = document.createDocumentFragment();
  fragment.append(createItem());
  fragment.append(createItem());
  fragment.append(createItem());
  list.append(fragment);
}

buttonOne.addEventListener('click', addItem);
buttonGroup.addEventListener('click', addGroup);